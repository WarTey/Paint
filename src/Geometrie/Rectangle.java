package Geometrie;

import java.awt.*;

public class Rectangle extends Figure {
    protected int largeur;
    protected int longueur;

    public Rectangle(int x, int y, Color c) {
        super(new Point(x, y), c);
        this.largeur = 0;
        this.longueur = 0;
    }

    public int getLargeur() {
        return this.largeur;
    }

    public int getLongeur() {
        return this.longueur;
    }

    public void setLargeur(int largeur) {
        this.largeur = largeur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public double getPerimetre() {
        return 2 * (this.largeur + this.longueur);
    }

    public double getSurface() {
        return this.largeur * this.longueur;
    }

    public void setBoundingBox(int hauteurBB, int largeurBB) {
        this.longueur = hauteurBB;
        this.largeur = largeurBB;
    }

    public void draw(Graphics g) {
        g.setColor(this.c);
        g.fillRect(this.origine.getX() + (this.largeur < 0 ? this.largeur : 0), this.origine.getY() + (this.longueur < 0 ? this.longueur : 0), Math.abs(this.largeur), Math.abs(this.longueur));
    }
}
