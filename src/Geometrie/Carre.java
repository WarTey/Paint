package Geometrie;

import java.awt.*;

public class Carre extends Rectangle {
    public Carre(int x, int y, Color c) { super(x, y, c); }

    public void setLargeur(int cote) {
        super.setLargeur(cote);
        super.setLongueur(cote);
    }

    public void setLongueur(int cote) {
        super.setLargeur(cote);
        super.setLongueur(cote);
    }

    public void setBoundingBox(int hauteurBB, int largeurBB) {
        if(Math.abs(hauteurBB) < Math.abs(largeurBB)) {
            this.longueur = hauteurBB;
            if ((hauteurBB < 0 && largeurBB > 0) || (hauteurBB > 0 && largeurBB < 0)) {
                this.largeur = -hauteurBB;
            } else {
                this.largeur = hauteurBB;
            }
        } else if(Math.abs(hauteurBB) > Math.abs(largeurBB)) {
            this.largeur = largeurBB;
            if((hauteurBB < 0 && largeurBB > 0) || (hauteurBB > 0 && largeurBB < 0)) {
                this.longueur = -largeurBB;
            } else {
                this.longueur = largeurBB;
            }
        }
    }
}
