package Geometrie;

import java.awt.*;

public abstract class Figure {
    protected Point origine;
    protected Color c;

    public Figure(Point origine) {
        this.origine = origine;
    }

    public Figure(Point origine, Color c) {
        this.origine = origine;
        this.c = c;
    }

    public abstract double getPerimetre();
    public abstract double getSurface();

    public abstract void setBoundingBox(int hauteurBB, int largeurBB);
    public abstract void draw(Graphics g);

    public String toString() {
        StringBuffer chaine = new StringBuffer();

        String coordX = new String("Coordonnées X : " + this.origine.getX());
        String coordY = new String("Coordonnées Y : " + this.origine.getY());

        chaine.append(coordX);
        chaine.append(System.lineSeparator());
        chaine.append(coordY);

        return chaine.toString();
    }
}
