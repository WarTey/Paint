package Geometrie;

import java.awt.*;

public class Ellipse extends Figure {
    protected int petitAxe;
    protected int grandAxe;

    public Ellipse(int x, int y, Color c) {
        super(new Point(x, y), c);
        this.petitAxe = 0;
        this.grandAxe = 0;
    }

    public int getPetitAxe() {
        return this.petitAxe;
    }

    public int getGrandAxe() {
        return this.grandAxe;
    }

    public void setPetitAxe(int petitAxe) {
        this.petitAxe = petitAxe;
    }

    public void setGrandAxe(int grandAxe) {
        this.grandAxe = grandAxe;
    }

    public double getPerimetre() { return Math.PI * (this.grandAxe + this.petitAxe); }

    public double getSurface() { return Math.PI * this.grandAxe * this.petitAxe / 4; }

    public void setBoundingBox(int hauteurBB, int largeurBB) {
        this.grandAxe = hauteurBB;
        this.petitAxe = largeurBB;
    }

    public void draw(Graphics g) {
        g.setColor(this.c);
        g.fillOval(this.origine.getX() + (this.petitAxe < 0 ? this.petitAxe : 0), this.origine.getY() + (this.grandAxe < 0 ? this.grandAxe : 0), Math.abs(this.petitAxe), Math.abs(this.grandAxe));
    }
}
