package Geometrie;

import java.awt.*;

public class Cercle extends Ellipse {
    public Cercle(int x, int y, Color c) { super(x, y, c); }

    public void setPetitAxe(int axe) {
        super.setPetitAxe(axe);
        super.setGrandAxe(axe);
    }

    public void setGrandAxe(int axe) {
        super.setPetitAxe(axe);
        super.setGrandAxe(axe);
    }

    public void setBoundingBox(int hauteurBB, int largeurBB) {
        if(Math.abs(hauteurBB) < Math.abs(largeurBB)) {
            this.grandAxe = hauteurBB;
            if ((hauteurBB < 0 && largeurBB > 0) || (hauteurBB > 0 && largeurBB < 0)) {
                this.petitAxe = -hauteurBB;
            } else {
                this.petitAxe = hauteurBB;
            }
        } else if(Math.abs(hauteurBB) > Math.abs(largeurBB)) {
            this.petitAxe = largeurBB;
            if((hauteurBB < 0 && largeurBB > 0) || (hauteurBB > 0 && largeurBB < 0)) {
                this.grandAxe = -largeurBB;
            } else {
                this.grandAxe = largeurBB;
            }
        }
    }
}
