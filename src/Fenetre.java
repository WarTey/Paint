import java.awt.*;          // Pour importer Color et Container
import java.awt.event.*;    // Pour importe la gestion des événements
import javax.swing.*;       // Pour importer tout le reste : JFrame, ...

public class Fenetre extends JFrame implements ActionListener {
    private Dessin monDessin;

    public Fenetre(String titre) {
        // On initialise la Fenetre (avec le titre) de la super classe
        super(titre);
        // Permet de pouvoir fermer totalement le programme
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Défini la localisation sur l'écran de la fenêtre
        setLocation(500, 250);
        // Défini la taille de la fenêtre
        setSize(1000, 500);

        // Container contient un BorderLayout
        // Remplace setLayout(new BorderLayout()); dans awt

        Container contentPane = getContentPane();

        // Initialise la barre de menu
        JMenuBar menu = buildMenuBar();
        // Affiche la barre de menu
        setJMenuBar(menu);

        // Initialise la zone de dessin
        monDessin = new Dessin();
        contentPane.add(monDessin, BorderLayout.CENTER);

        // Initialise le panneau de gestion
        JPanel panneau = new JPanel();
        //panneau.setLayout(new GridLayout(1,2));

        // Initialise le panneau de couleurs et le panneau d'outils
        // Place ces deux panneaux dans le panneau de gestion
        panneau.add(buildPanneauCouleur());
        panneau.add(buildPanneauOutil());

        // Positionne le panneau de gestion au sud
        contentPane.add(panneau,"South");

        setVisible(true);
    }

    public JMenuBar buildMenuBar() {
        // Initialise la barre de menu
        JMenuBar menuBar = new JMenuBar();

        // Initialise le menu 'Fichier'
        JMenu fichier = new JMenu("Fichier");

        // Ajoute le sous-menu 'Nouveau' au menu 'Fichier'
        fichier.add(buildOptionMenuBar("Nouveau"));

        // Ajoute le sous-menu 'Ouvrir' au menu 'Fichier'
        fichier.add(buildOptionMenuBar("Ouvrir"));

        // Ajoute le sous-menu 'Enregistrer' au menu 'Fichier'
        fichier.add(buildOptionMenuBar("Enregistrer", KeyEvent.VK_S));

        // Sépare d’un trait
        fichier.addSeparator();

        // Ajoute le sous-menu 'Quitter' au menu 'Fichier'
        fichier.add(buildOptionMenuBar("Quitter"));

        // Place l'onglet 'Fichier' dans la barre de menu
        menuBar.add(fichier);

        // Initialise le menu 'A propos'
        JMenu aPropos = new JMenu("A propos");

        // Ajoute le sous-menu 'A propos' au menu 'Auteurs'
        aPropos.add(buildOptionMenuBar("Auteurs"));

        // Place l'onglet 'A propos' dans la barre de menu
        menuBar.add(aPropos);

        return menuBar;
    }

    public JMenuItem buildOptionMenuBar(String texte) {
        // Défini un sous-menu contenant le texte en paramètre
        JMenuItem sousMenu = new JMenuItem(texte);
        // Ajoute une écoute sur le bouton
        sousMenu.addActionListener(this);

        return sousMenu;
    }

    public JMenuItem buildOptionMenuBar(String texte, int keyCode) {
        // Défini un sous-menu contenant le texte en paramètre
        JMenuItem sousMenu = new JMenuItem(texte);
        // Ajoute une écoute sur le bouton
        sousMenu.addActionListener(this);
        // Raccourci
        sousMenu.setAccelerator(KeyStroke.getKeyStroke(keyCode, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(),false));

        return sousMenu;
    }

    public JPanel buildPanneauOutil() {
        // Initialise le panneau d'outils
        JPanel panneauOutil = new JPanel();
        // Initialise le panneau avec 2 lignes et 2 colonnes
        panneauOutil.setLayout(new GridLayout(2,2));

         // Ajoute le bouton 'Ellipse' dans la panneau d'outils
        panneauOutil.add(buildBoutonOutil("Ellipse"));

        // Ajoute le bouton 'Ellipse' dans la panneau d'outils
        panneauOutil.add(buildBoutonOutil("Cercle"));

        // Ajoute le bouton 'Ellipse' dans la panneau d'outils
        panneauOutil.add(buildBoutonOutil("Carre"));

        // Ajoute le bouton 'Ellipse' dans la panneau d'outils
        panneauOutil.add(buildBoutonOutil("Rectangle"));

        return panneauOutil;
    }

    public JButton buildBoutonOutil(String texte) {
        // Défini un bouton contenant le texte en paramètre
        JButton bouton = new JButton(texte);
        // Ajoute une écoute sur le bouton
        bouton.addActionListener(this);

        return bouton;
    }

    public JPanel buildPanneauCouleur() {
        // Initialise le panneau de couleurs
        JPanel panneauCouleur = new JPanel();
        // Initialise le panneau avec 2 lignes et 4 colonnes
        panneauCouleur.setLayout(new GridLayout(2,4));

        // Ajoute le bouton 'Noir' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Noir", Color.black, Color.white));

        // Ajoute le bouton 'Rouge' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Rouge", Color.red, Color.black));

        // Ajoute le bouton 'Vert' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Vert", Color.green, Color.black));

        // Ajoute le bouton 'Bleu' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Bleu", Color.blue, Color.white));

        // Ajoute le bouton 'Jaune' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Jaune", Color.yellow, Color.black));

        // Ajoute le bouton 'Rose' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Rose", Color.pink, Color.black));

        // Ajoute le bouton 'Magenta' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Magenta", Color.magenta, Color.black));

        // Ajoute le bouton 'Orange' dans le panneau de couleurs
        panneauCouleur.add(buildBoutonCouleur("Orange", Color.orange, Color.black));

        return panneauCouleur;
    }

    public JButton buildBoutonCouleur(String texte, Color couleurBouton, Color couleurTexte) {
        // Défini un bouton contenant le texte en paramètre
        JButton bouton = new JButton(texte);
        // Ajoute une écoute sur le bouton
        bouton.addActionListener(this);
        // Défini la couleur du bouton
        bouton.setBackground(couleurBouton);
        // Défini la couleur de la police du bouton
        bouton.setForeground(couleurTexte);

        return bouton;
    }

    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        if(cmd.equals("Rectangle") || cmd.equals("Carre") || cmd.equals("Ellipse") || cmd.equals("Cercle")) {
            monDessin.setNomFigure(cmd);
        } else if(cmd.equals("Noir")) {
            monDessin.setC(Color.black);
        } else if(cmd.equals("Rouge")) {
            monDessin.setC(Color.red);
        } else if(cmd.equals("Vert")) {
            monDessin.setC(Color.green);
        } else if(cmd.equals("Bleu")) {
            monDessin.setC(Color.blue);
        } else if(cmd.equals("Jaune")) {
            monDessin.setC(Color.yellow);
        } else if(cmd.equals("Rose")) {
            monDessin.setC(Color.pink);
        } else if(cmd.equals("Magenta")) {
            monDessin.setC(Color.magenta);
        } else if(cmd.equals("Orange")) {
            monDessin.setC(Color.orange);
        } else if(cmd.equals("Nouveau")) {
            for(int i = monDessin.getListe().size() - 1; i >= 0; i--) {
                monDessin.getListe().remove(monDessin.getListe().get(i));
                System.out.println(i);
            }
            repaint();
        } else if(cmd.equals("Auteurs")) {
            JOptionPane.showMessageDialog(new JOptionPane(),"Guillaume Blanc de Lanaute");
        } else if(cmd.equals("Quitter")) {
            System.exit(0);
        } else {
            System.err.println(cmd);
        }
    }
}
