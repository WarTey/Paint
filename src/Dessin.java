import Geometrie.*;         // Pour importer le package 'Geometrie'
import Geometrie.Rectangle; // Pour importe la classe Rectangle du package 'Geometrie'
                            // Et ne pas confondre avec la classe Rectangle par défaut

import java.awt.*;          // Pour importer Color et Container
import java.awt.event.*;    // Pour importe la gestion des événements
import javax.swing.*;       // Pour importer tout le reste : JFrame, ...
import java.util.ArrayList; // Pour manipuler ArrayList

public class Dessin extends JPanel implements MouseListener, MouseMotionListener {
    private ArrayList<Figure> liste;
    private Color c;
    private String nomFigure;
    private int x, y;

    public Dessin() {
        // On appel le constructeur de la super classe
        super();
        // On initialise une liste vide
        this.liste = new ArrayList();
        // On défini la couleur par défaut sur noir
        this.c = Color.black;
        // On défini la figure par défaut sur rectangle
        this.nomFigure = "Rectangle";
        // On initialise les écouteurs de la souris
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void paintComponent(Graphics g) {
        // Appel de la fonction paintComponent de la super classe
        super.paintComponent(g);
        // On défini la couleur de fond sur blanc
        this.setBackground(Color.white);
        // On utilise la couleur choisie par l'utilisateur pour tracer
        g.setColor(this.c);
        // Dessine toutes les figures de la liste
        for(Figure f : this.liste) {
            f.draw(g);
        }
    }

    public void setC(Color c) { this.c = c; }

    public void setNomFigure(String nomFigure) { this.nomFigure = nomFigure; }

    public ArrayList<Figure> getListe() { return this.liste; }

    public void mouseClicked(MouseEvent e) { }

    public void mousePressed(MouseEvent e) {
        // Enregistre les coordonnées du clic
        this.x = e.getX();
        this.y = e.getY();
        // Initialise la première figure selon les coordonnées précédentes
        // Et selon la figure choisie
        initFigure();
        // Redessine la fenêtre
        repaint();
    }

    public void mouseReleased(MouseEvent e) { }

    public void mouseEntered(MouseEvent e) { }

    public void mouseExited(MouseEvent e) { }

    public void mouseDragged(MouseEvent e) {
        // Change les coordonnées de la dernière figure de la liste
        this.liste.get(this.liste.size() - 1).setBoundingBox(e.getY() - this.y, e.getX() - this.x);
        // Redessine la fenêtre
        repaint();
    }

    public void mouseMoved(MouseEvent e) { }

    public void initFigure() {
        if(this.nomFigure.equals("Rectangle")) {
            // Initialisation du rectangle
            Rectangle newRect = new Rectangle(this.x, this.y, this.c);
            this.liste.add(newRect);
        } else if(this.nomFigure.equals("Carre")) {
            // Initialisation du Carré
            Carre newCarr = new Carre(this.x, this.y, this.c);
            this.liste.add(newCarr);
        } else if(this.nomFigure.equals("Ellipse")) {
            // Initialisation du ellipse
            Ellipse newElli = new Ellipse(this.x, this.y, this.c);
            this.liste.add(newElli);
        } else if(this.nomFigure.equals("Cercle")) {
            // Initialisation du cercle
            Cercle newCerc = new Cercle(this.x, this.y, this.c);
            this.liste.add(newCerc);
        }
    }
}
